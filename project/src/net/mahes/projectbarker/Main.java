package net.mahes.projectbarker;

import java.util.ArrayList;
import java.util.List;


public class Main {

	public static final boolean DEBUG = false;
	
	public static void main(String[] args) {
		for (int length = 35; length < 100; ++length) {
			System.out.println();
			System.out.println(length+":");
			List<boolean[]> codes = createBarkerCodes(length);
	
			for (boolean[] c : codes) {
				//printBooleanArray(c, "-1", "+1", " ");
				printBooleanArray(c, "_", "#", " ");
			}
		}
	}
	
	static boolean checkIfRelated(boolean[] a, boolean[] c) {
		boolean[] b = c.clone();
		for (int i = 0; i < b.length; ++i) {
			if (arrayEquals(a, b)) {
				return true;
			}
			rotateArray(b);
		}
		invertArray(b);
		for (int i = 0; i < b.length; ++i) {
			if (arrayEquals(a, b)) {
				return true;
			}
			rotateArray(b);
		}
		mirrorArray(b);
		for (int i = 0; i < b.length; ++i) {
			if (arrayEquals(a, b)) {
				return true;
			}
			rotateArray(b);
		}
		invertArray(b);
		for (int i = 0; i < b.length; ++i) {
			if (arrayEquals(a, b)) {
				return true;
			}
			rotateArray(b);
		}
		return false;
	}
	
	static void printBooleanArray(boolean[] array, String sTrue, String sFalse, String separator) {
		for (int i = 0; i < array.length; ++i) {
			System.out.print((array[i] ? sTrue : sFalse) + separator);
		}
		System.out.println();
	}

	static List<boolean[]> createBarkerCodes(int length) {
		List<boolean[]> codes = new ArrayList<boolean[]>();
		
		outer:
		for (long i = 0; ; ++i) {
			boolean[] code = createPossibleBarkerCode(length, i);
			if (code == null) {
				break outer;
			}
			if (!checkIfBarkerCode(code)) {
				continue outer;
			}
			for (boolean[] oldCode : codes) {
				if (checkIfRelated(code, oldCode)) {
					continue outer;
				}
			}
			codes.add(code);
		}
		
		return codes;
	}
	
	static boolean[] createPossibleBarkerCode(int length, long nr) {
		long max = (long)Math.pow(2, length);
		if (nr >= max) {
			return null;
		}
		boolean[] array = new boolean[length];
		
		for (int i = 0; i < length; ++i) {
			max /= 2;
			array[i] = nr >= (max);
			nr %= max;
		}
		
		return array;
	}
	
	static boolean checkIfBarkerCode(boolean[] a) {
		int rCode = getCodeProduct(a, a);
		if (DEBUG) System.out.print("A: "+rCode+" B: ");
		if (rCode != a.length) {
			if (DEBUG) System.out.println();
			return false;
		}
		
		boolean[] b = a.clone();
		
		for (int i = 0; i < a.length-1; ++i) {
			rotateArray(b);
			rCode = Math.abs(getCodeProduct(a, b));
			if (DEBUG) System.out.print(rCode+" ");
			if (rCode > 1) {
				if (DEBUG) System.out.println();
				return false;
			}
		}
		
		if (DEBUG) System.out.println();
		return true;
	}
	
	static void mirrorArray(boolean[] a) {
		boolean t;
		for (int i = 0; i < a.length/2; ++i) {
			t = a[i];
			a[i] = a[a.length-i-1];
			a[a.length-i-1] = t;
		}
	}
	
	static void rotateArray(boolean[] a) {
		boolean first = a[0];
		for (int i = 0; i < a.length-1; ++i) {
			a[i] = a[i+1];
		}
		a[a.length-1] = first;
	}
	
	static void invertArray(boolean[] a) {
		for (int i = 0; i < a.length; ++i) {
			a[i] = !a[i];
		}
	}
	
	static boolean arrayEquals(boolean[] a, boolean[] b) {
		if (a.length != b.length) {
			return false;
		}
		for (int i = 0; i < a.length; ++i) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}
	
	static int getCodeProduct(boolean[] a, boolean[] b) {
		int res = 0;
		for (int i = 0; i < a.length; ++i) {
			res += (a[i] ? 1 : -1) * (b[i] ? 1 : -1); 
		}
		return res;
	}
}